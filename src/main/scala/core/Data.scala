package core

/**
 * Errors used for internal purposes
 */
object Data {
  sealed trait AppError extends Exception

  case object EntryDoesntExists          extends AppError
  case object StrangeError               extends AppError
  case object TimeoutError               extends AppError
  case object ServiceUnavailable         extends AppError
  case class ValidationError(msg:String) extends AppError

  /**
   * Request which service receives from the end user
   */
  case class InitialRequest(name: String, creditScore: Int, salary: Int)

  /**
   * Entry which is returned to the end user. Response will be List[FinalResponseEntry]
   */
  case class FinalResponseEntry(provider: String, name: String, apr: Double, cardScore: Double)


}
