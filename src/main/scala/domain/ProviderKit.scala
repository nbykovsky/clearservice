package domain

import cats.MonadError
import cats.syntax.applicativeError._
import cats.syntax.flatMap._
import cats.syntax.functor._
import core.Data.{EntryDoesntExists, FinalResponseEntry, InitialRequest}
import domain.Fetcher.{Request, Response}

/**
 * Aggregates all the provider specific modules along with logic.
 *
 * F[_] could be any type *->* which support MonadError interface
 */
trait ProviderKit[F[_]] {

  /**
   * Type of provider specific request
   */
  type Req <: Request
  /**
   * Type of provicer specific response
   */
  type Resp <: Response


  def providerName: String

  def codec: Codec[F, Req, Resp]

  def fetcher: Fetcher[F, Req, Resp]

  def cacher: Cacher[F, Req, Resp]

  /**
   * Accepts request in initial (user) format and tries to fetch a result from cache if it's not there,
   * requests external service and writes result into the cache.
   * If cache fails for some reason (except EntryDoesntExists), goes to the external service
   * and don't write anything into the cache
   *
   * Returns result in end user format.
   */
  def fetchScore(initReq: InitialRequest)(implicit me: MonadError[F, Throwable]): F[List[FinalResponseEntry]] =
    for {
      req   <- codec.encode(initReq)
      fetch =  cacher.read(req)
      resp  <- fetch.recoverWith {
        case EntryDoesntExists =>
          for {
            remote <- fetcher.fetch(req)
            _      <- cacher.write(req, remote)
          } yield remote
        case _ =>
          fetcher.fetch(req)
      }
      dec   <-  codec.decode(resp)
    } yield dec

}
