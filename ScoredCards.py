import time

from flask import jsonify

from flask import Flask

app = Flask(__name__)

resp = [{
    "card": "ScoredCard Builder",
    "apr": 19.4,
    "approvalRating": 0.8}]



@app.route('/score', methods=['POST'])
def hello_world():
    # time.sleep(11)
    return jsonify(resp), 201


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='9000', debug=True)