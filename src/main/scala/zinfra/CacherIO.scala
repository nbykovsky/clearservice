package zinfra

import cats.effect.IO
import core.Data.EntryDoesntExists
import domain.Cacher
import domain.Fetcher.{Request, Response}

/**
 * Dummy implementation of cache: don't cache anything :)
 *
 * For production I would recommend to use external cache like redis or to use queue to keep cache size constant
 */
class CacherIO[Req <: Request, Resp <: Response] extends Cacher[IO,Req, Resp]{

  def write(req: Req, resp: List[Resp]): IO[Unit] = IO.pure(())

  def read(req: Req): IO[List[Resp]] = IO.raiseError[List[Resp]](EntryDoesntExists)

}
