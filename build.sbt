name := "clearscore"

version := "0.1"

scalaVersion := "2.12.8"

scalacOptions ++= Seq("-deprecation", "-feature")
scalacOptions += "-Ypartial-unification"
scalacOptions += "-language:higherKinds"
scalacOptions += "-language:postfixOps"

val http4sVersion = "0.20.10"

libraryDependencies ++= Seq(

  "org.scalatest" %% "scalatest" % "3.0.4",
  "org.typelevel" %% "cats-core" % "2.0.0-M1",
  "org.typelevel" %% "cats-effect" % "1.3.1",

  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.slf4j" % "slf4j-simple" % "1.7.21",
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "io.circe" %% "circe-generic" % "0.11.1",
  "io.circe" %% "circe-literal" % "0.11.1"
)

resolvers += Resolver.sonatypeRepo("releases")

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3")

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

mainClass in assembly := Some("run")

assemblyJarName := "clearscore.jar"
