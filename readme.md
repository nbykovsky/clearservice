## Score aggregator
#### Design 
The Service is implemented if functional programming style using IO monad (cats-effect library and http4s web framework). 
It supports any numbers of score providers. A new provider might be added by implementing ProviderKit interface and 
adjusting run.scala file. Also it has placeholder for cacher.

Project is built using layered (onion) structure  
**Layer 0** represented by `core` package includes general definitions and utility functions. Self sufficient doesnt
depend on other layers  

**Layer 1** `domain` - maximum abstracted layer which implements provider specific business logic without locking to 
effect type and any implementations of interactions with outside world. It might be uses with any Effect 
which has MonadError implementation (IO, Future, Either, Try etc...). Depends only on **Layer 0**    

**Layer 2** `service` - application specific implementation of business logic. Doesn't depend on any specific 
 implementations of input/output interfaces. Depends on **Layer 0** and **Layer 1**       
 
**Layer 3** `zinfra` - top infrastructure layer where we implement all the abstractions and put everything together. Here
we stick with IO monad from cats-effect and use http4s web framework


#### Deployment
Application is tested with Java 8 and Java 12   

Before deploying to production this service requires logging (currently it outputs everything into stdout)
and proper implementation of caching with redis or in-memory cache with constant size or used memory.

Since this service is designed to be stateless it could be easily scaled by putting multiple instances behind 
load balancer. More specifically it could be packaged into docker container and orchestrated by docker swarm or kubernetes, 
it this case orchestration tool would handle service discovery and load balancing.

#### Building and Testing
to build .jar file run `sbt assembly`, file will be available in target scala-2.12 folder   
to execute unit tests run `sbt test`    
to run application execute `sh start.sh` from the command line  