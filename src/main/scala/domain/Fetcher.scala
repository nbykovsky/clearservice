package domain

import domain.Fetcher.{Request, Response}

/**
 * This trait is responsible for requesting data from a specific provider
 *
 * F[_] supposed to support MonadError interface, Req and Resp - types of request and responses from providers
 */
trait Fetcher[F[_], Req <: Request, Resp <: Response] {

  /**
   * Request data in provider specific format
   */
  def fetch(req: Req): F[List[Resp]]
}

object Fetcher {

  /**
   * Abstract request and response from provider
   */
  trait Request
  trait Response

}
