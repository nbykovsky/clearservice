import cats.MonadError
import cats.effect.{ContextShift, ExitCode, IO, IOApp, Timer}
import core.Data.AppError
import domain.ProviderKit
import org.http4s.Uri
import service.Service
import zinfra.providerkits._
import zinfra.EndPointsIO
import scala.concurrent.duration._
import scala.util.Try
import cats.effect.IO._
import cats._
import cats.implicits._



object run extends IOApp with EndPointsIO  {

  private val HTTP_PORT = Try(sys.env("HTTP_PORT")).map(_.toInt).filter(p => p > 0 && p <= 65535).toEither
  private val CSCARDS_ENDPOINT = Try(sys.env("CSCARDS_ENDPOINT")).map(Uri.unsafeFromString).toEither
  private val SCOREDCARDS_ENDPOINT = Try(sys.env("SCOREDCARDS_ENDPOINT")).map(Uri.unsafeFromString).toEither

  def constructor(port: Int, cCardsUri: Uri, csoreCardsUri: Uri): IO[ExitCode] = {
    val providerKits = List(
      CSCardsProviderKitIO(cCardsUri, 10.seconds),
      ScoredCardsProviderKitIO(csoreCardsUri, 10.seconds)
    )
    val serviceIO = new Service[IO](providerKits)
    runner(port, serviceIO)
  }

  def run(args: List[String]): IO[ExitCode] = {
    val go: IO[ExitCode] = for {
      port     <- fromEither(HTTP_PORT)
      _        <- IO(println(s"HTTP_PORT: $port"))
      ccards   <- fromEither(CSCARDS_ENDPOINT)
        _      <- IO(println(s"CSCARDS_ENDPOINT: $ccards"))
      score    <- fromEither(SCOREDCARDS_ENDPOINT)
      _        <- IO(println(s"SCOREDCARDS_ENDPOINT: $score"))
      r        <- constructor(port, ccards, score)
    } yield r
    go.handleErrorWith(e => IO(println(s"ERROR: [APP] Init $e")) *> IO(ExitCode.Error))
    }

  }

