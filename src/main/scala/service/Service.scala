package service

import cats.{MonadError, Parallel}
import core.Data.{FinalResponseEntry, InitialRequest}
import domain.ProviderKit

/**
 * Main implementation of business logic. Supports arbitrary number of providers (accepts those in providerKits variable)
 */
class Service[F[_]](providerKits: List[ProviderKit[F]])(implicit me: MonadError[F, Throwable]) {

  import cats.implicits._

  type Result = Either[(String, Throwable), List[FinalResponseEntry]]

  /**
   * Separating interaction with providers to be able to mock those during testing
   */
  def getResult(initReq: InitialRequest): List[(String, F[List[FinalResponseEntry]])] =
    providerKits.map(pk => (pk.providerName, pk.fetchScore(initReq)))

  /**
   * Requests results from all the providers in parallel. This method should never fail: successful requests will be
   * returned in the second item, unsuccessful fill be represented as map ProviderName -> Error in the first item.
   *
   * If all requests failed second item will be empty list
   *
   */
  def fetchResult[G[_]](initReq: InitialRequest)(implicit pr: Parallel[F, G]): F[(Map[String, Throwable], List[FinalResponseEntry])] = {
    import cats.syntax.either._
    // here we request external service (creating task) parSequence makes it parallel
    val combined = getResult(initReq).map{p =>
      p._2.map(_.asRight:Result).recover {case e => (p._1, e).asLeft:Result}}.parSequence

    // separate successful and failure attempts
    val transformed = combined.map {res =>
      res.partition((x: Either[(String, Throwable), List[FinalResponseEntry]]) => x.isLeft)}
      .map(res => (res._1.collect {case Left(l) => l} toMap, res._2.collect {case Right(r) => r} flatten))

    // sort output by descending score (as per requirement)
    transformed.map(x => (x._1, x._2.sortBy(_.cardScore).reverse))
  }

}
