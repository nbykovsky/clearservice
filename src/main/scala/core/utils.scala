package core

import cats.effect.{ContextShift, IO, Timer}
import core.Data.AppError

import scala.concurrent.duration.FiniteDuration
import scala.math.pow


/**
 * Different utilities method shared by modules
 */
object utils {

  /**
   * Fails IO computation if it hasn't been finished within time slot
   */
  def timeout[A](after: FiniteDuration, error: AppError)(fa: IO[A])
                (implicit timer: Timer[IO], cs: ContextShift[IO]): IO[A] = {
    IO.race(fa, timer.sleep(after)).flatMap {
      case Left(a) => IO.pure(a)
      case Right(_) => IO.raiseError(error)
    }
  }


  /**
   * Rounds the number down to the given number of decimals
   */
  def ceil(d: Double, n: Int): Double = Math.floor(d * pow(10, n)) / pow(10, n)

}
