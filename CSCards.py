import time

from flask import jsonify

from flask import Flask

app = Flask(__name__)

resp = [
    {"cardName": "SuperSaver Card",
     "apr": 21.4,
     "eligibility": 6.3},
    {"cardName": "SuperSpender Card",
     "apr": 19.2,
     "eligibility": 5.0}]


@app.route('/score', methods=['POST'])
def hello_world():
    # time.sleep(9)
    return jsonify(resp), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000', debug=True)