package domain

import cats.effect.IO
import core.Data.{EntryDoesntExists, TimeoutError}
import domain.Fetcher.{Request, Response}
import javax.xml.crypto.dsig.spec.ExcC14NParameterSpec
import org.scalatest.WordSpec
import core.Data._

class ProviderKitSpec extends WordSpec {

  trait TestProviderKit extends ProviderKit[IO] {

    case class InMemRequest() extends Request

    case class InMemResponseEntry() extends Response

    type Req = InMemRequest
    type Resp = InMemResponseEntry

    def providerName: String = "test provider"

    def codec: Codec[IO, Req, Resp] = new Codec[IO, Req, Resp] {
      def encode(initReq: InitialRequest): IO[Req] = IO.pure(InMemRequest())
      def decode(resp: List[Resp]): IO[List[FinalResponseEntry]] = IO.pure(List(FinalResponseEntry("test provider", "", 10.4, 10.4)))
    }

    def fetcher: Fetcher[IO, Req, Resp] = new Fetcher[IO, Req, Resp]{
      def fetch(req: InMemRequest): IO[List[InMemResponseEntry]] = IO.pure(List(InMemResponseEntry()))
    }

  }

  "A Provider kit" should {
    "Read result from cache if it exists (don't call external service and don't write cache again)" in {
      var called = false
      val testProviderKit: TestProviderKit = new TestProviderKit {

        override val fetcher: Fetcher[IO, InMemRequest, InMemResponseEntry] = new Fetcher[IO, Req, Resp]{
          // we don't expect it called
          def fetch(req: Req): IO[List[Resp]] = IO.raiseError(new Exception("error"))
        }

        val cacher: Cacher[IO, InMemRequest, InMemResponseEntry] = new Cacher[IO, InMemRequest, InMemResponseEntry] {
          def write(req: Req, resp: List[Resp]): IO[Unit] = IO.raiseError(new Exception("Cache shouldn't be written"))

          def read(req: Req): IO[List[Resp]] =
            if (req == InMemRequest()) {
              IO.pure {called = true; List(InMemResponseEntry())}
            } else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }
      }

      val result = testProviderKit.fetchScore(InitialRequest("Ivan", 10, 20))
      assert(result.unsafeRunSync() == List(FinalResponseEntry("test provider", "", 10.4, 10.4)) && called )

    }

    "Read result from external service if it doesn't exists in cache and write to cache" in {
      var externalCalled = false
      var cacheWriteCalled = false
      var cacheReadCalled = false
      val testProviderKit: TestProviderKit = new TestProviderKit {

        override val fetcher: Fetcher[IO, InMemRequest, InMemResponseEntry] = new Fetcher[IO, Req, Resp]{
          def fetch(req: Req): IO[List[Resp]] =
            if(req == InMemRequest()) IO.pure {externalCalled = true; List(InMemResponseEntry())}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }

        val cacher: Cacher[IO, InMemRequest, InMemResponseEntry] = new Cacher[IO, InMemRequest, InMemResponseEntry] {
          def write(req: Req, resp: List[Resp]): IO[Unit] =
            if (req == InMemRequest()) IO.pure {cacheWriteCalled = true;()}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))

          def read(req: Req): IO[List[Resp]] =
            if (req == InMemRequest()) {
              IO.pure {cacheReadCalled = true}.flatMap(_ => IO.raiseError(EntryDoesntExists))
            } else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }
      }

      val result = testProviderKit.fetchScore(InitialRequest("Ivan", 10, 20))
      assert(result.unsafeRunSync() == List(FinalResponseEntry("test provider", "", 10.4, 10.4)), "Unexpected result")
      assert(externalCalled, "External API wasn't called")
      assert(cacheReadCalled, "Value wasn't read from cache")
      assert(cacheWriteCalled, "Value wasn't written into the cache")

    }

    "Read result from external service if cache fails and don't write it to cache" in {
      var externalCalled = false
      var cacheReadCalled = false
      val testProviderKit: TestProviderKit = new TestProviderKit {

        override val fetcher: Fetcher[IO, InMemRequest, InMemResponseEntry] = new Fetcher[IO, Req, Resp]{
          def fetch(req: Req): IO[List[Resp]] =
            if(req == InMemRequest()) IO.pure {externalCalled = true; List(InMemResponseEntry())}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }

        val cacher: Cacher[IO, InMemRequest, InMemResponseEntry] = new Cacher[IO, InMemRequest, InMemResponseEntry] {
          def write(req: Req, resp: List[Resp]): IO[Unit] = IO.raiseError(new Exception("This shouldn't be called"))

          def read(req: Req): IO[List[Resp]] =
            if (req == InMemRequest()) IO.pure {cacheReadCalled = true} flatMap{_ => IO.raiseError(new Exception("Random exception"))}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }
      }

      val result = testProviderKit.fetchScore(InitialRequest("Ivan", 10, 20))
      assert(result.unsafeRunSync() == List(FinalResponseEntry("test provider", "", 10.4, 10.4)), "Unexpected result")
      assert(externalCalled, "External API wasn't called")
      assert(cacheReadCalled, "Value wasn't read from cache")

    }

    "If everything failed we should get exception from remote server" in {
      var externalCalled = false
      var cacheReadCalled = false

      case object FetcherError extends Exception
      case object CacherError extends Exception

      val testProviderKit: TestProviderKit = new TestProviderKit {

        override val fetcher: Fetcher[IO, InMemRequest, InMemResponseEntry] = new Fetcher[IO, Req, Resp]{
          def fetch(req: Req): IO[List[Resp]] =
            if(req == InMemRequest()) IO.pure {externalCalled = true} flatMap {_ => IO.raiseError(FetcherError)}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }

        val cacher: Cacher[IO, InMemRequest, InMemResponseEntry] = new Cacher[IO, InMemRequest, InMemResponseEntry] {
          def write(req: Req, resp: List[Resp]): IO[Unit] = IO.raiseError(new Exception("This shouldn't be called"))

          def read(req: Req): IO[List[Resp]] =
            if (req == InMemRequest()) IO.pure {cacheReadCalled = true} flatMap{_ => IO.raiseError(CacherError)}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }
      }

      val result = testProviderKit.fetchScore(InitialRequest("Ivan", 10, 20))
      assertThrows[FetcherError.type] (result.unsafeRunSync())
      assert(externalCalled, "External API wasn't called")
      assert(cacheReadCalled, "Value wasn't read from cache")
    }

    "If use doesn't exists in cache and external call failed we should get Fetcher error" in {
      var externalCalled = false
      var cacheReadCalled = false

      case object FetcherError extends Exception

      val testProviderKit: TestProviderKit = new TestProviderKit {

        override val fetcher: Fetcher[IO, InMemRequest, InMemResponseEntry] = new Fetcher[IO, Req, Resp]{
          def fetch(req: Req): IO[List[Resp]] =
            if(req == InMemRequest()) IO.pure {externalCalled = true} flatMap {_ => IO.raiseError(FetcherError)}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }

        val cacher: Cacher[IO, InMemRequest, InMemResponseEntry] = new Cacher[IO, InMemRequest, InMemResponseEntry] {
          def write(req: Req, resp: List[Resp]): IO[Unit] = IO.raiseError(new Exception("This shouldn't be called"))

          def read(req: Req): IO[List[Resp]] =
            if (req == InMemRequest()) IO.pure {cacheReadCalled = true} flatMap{_ => IO.raiseError(EntryDoesntExists)}
            else IO.raiseError(new Exception(s"Wrong request current: $req expected ${InMemRequest()}"))
        }
      }

      val result = testProviderKit.fetchScore(InitialRequest("Ivan", 10, 20))
      assertThrows[FetcherError.type] (result.unsafeRunSync())
      assert(externalCalled, "External API wasn't called")
      assert(cacheReadCalled, "Value wasn't read from cache")
    }
  }

}
