package zinfra

import cats.effect.{ContextShift, IO, Timer}
import cats.syntax.all._
import core.Data.TimeoutError
import core.utils._
import domain.Fetcher
import domain.Fetcher.{Request, Response}
import org.http4s.client.blaze._
import org.http4s.client.dsl.io._
import org.http4s.dsl.io._
import org.http4s.{EntityDecoder, EntityEncoder, Uri}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{FiniteDuration, _}

/**
 * http4s implementation of fetcher. Accepts Endpoint of score provider and timeout value
 */
class WebFetcherIO[Req <: Request, Resp <: Response](endpoint: Uri, tmt: FiniteDuration)
                                                    (implicit enc: EntityEncoder[IO, Req],
                                                     dec: EntityDecoder[IO, List[Resp]]) extends Fetcher[IO, Req, Resp]{

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)

  /**
   * Number of retries in case of ConnectException
   */
  private val NUMBER_ATTEMPTS = 3
  private val INITIAL_DELAY = 1.seconds

  /**
   * Retries computation given number of times, accepts initial delay between the first and second attempts,
   * further delays will be doubled
   *
   * retried only if ConnectException
   */
  def retry[A](initialDelay: FiniteDuration, maxRetries: Int)(ioa: IO[A])
                             (implicit timer: Timer[IO]):  IO[A] = {
    ioa.handleErrorWith {
        case e: java.net.ConnectException =>
          if (maxRetries > 1) IO.sleep(initialDelay) *> retry(initialDelay * 2, maxRetries - 1)(ioa)
          else IO.raiseError(e)
        case e => IO.raiseError(e)
      }
  }

  /**
   * Tries to fetch data from provider if it fails with ConnectException retries NUMBER_ATTEMPTS times.
   * If request hasn't been finished within tmt timeout, terminates with TimeoutError
   */
  def fetch(req: Req): IO[List[Resp]] =
    timeout[List[Resp]](tmt, TimeoutError) _ andThen
      retry(INITIAL_DELAY, NUMBER_ATTEMPTS)  apply
      BlazeClientBuilder[IO](global).resource.use(_.expect[List[Resp]](POST(req, endpoint)))


}
