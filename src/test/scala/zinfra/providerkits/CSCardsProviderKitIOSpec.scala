package zinfra.providerkits

import core.Data._
import org.http4s.Uri
import org.scalatest.WordSpec
import zinfra.providerkits.CSCardsProviderKitIO.CSCardsResponseEntry

import scala.concurrent.duration._

class CSCardsProviderKitIOSpec extends WordSpec{

  val kit = CSCardsProviderKitIO(Uri.uri("https://google.com"), 10.seconds)

  "CSCardsProviderKitIO" should {
    "correctly decode normal response from provider" in {
      val result = kit.codec.decode(
        List(CSCardsResponseEntry(21.4, "SuperSaver Card", 6.3),
        CSCardsResponseEntry(19.2, "SuperSpender Card", 5.0)))
      val expected = List(
        FinalResponseEntry("CSCards", "SuperSaver Card", 21.4, 0.137),
        FinalResponseEntry("CSCards", "SuperSpender Card", 19.2, 0.135)
      )
      assert(result.unsafeRunSync() == expected)
    }
   "assign cardScore 0 if apr is 0" in {
     val result = kit.codec.decode(List(CSCardsResponseEntry(0, "SuperSaver Card", 6.3)))
     val expected = List(FinalResponseEntry("CSCards", "SuperSaver Card", 0, 0))

     assert(result.unsafeRunSync() == expected)
   }
  }
}
