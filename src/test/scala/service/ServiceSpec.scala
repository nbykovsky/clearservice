package service

import cats.effect.{ContextShift, IO, Timer}
import core.Data.{ServiceUnavailable, TimeoutError}
import core.Data._
import domain.ProviderKit
import org.scalatest.WordSpec

import scala.concurrent.duration._

class ServiceSpec extends WordSpec{

  import scala.concurrent.ExecutionContext.global
  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)

  "fetch result function" should {
    "return correctly sorted values if all requests are successful" in {
      val service = new Service[IO](List()) {
        override def getResult(initReq: InitialRequest): List[(String, IO[List[FinalResponseEntry]])] = List(
          ("A", IO(List(FinalResponseEntry("A","a", 4, 1), FinalResponseEntry("A","b", 4, 5)))),
          ("B", IO(List(FinalResponseEntry("B","a", 4, 3), FinalResponseEntry("B","b", 4, 2)))),
          ("C", IO(List(FinalResponseEntry("C","a", 4, 4), FinalResponseEntry("C","b", 4, 6))))
        )
      }
      val result: (Map[String, Throwable], List[FinalResponseEntry]) = service.fetchResult(InitialRequest("x", 100, 100)).unsafeRunSync()
      assert(result._1.isEmpty)
      assert(result._2.map(_.cardScore) == List(6.0,5.0,4.0,3.0,2.0,1.0))

    }

    "run requests in parallel during evaluation" in {

      var start1: Option[Long] = None
      var start2: Option[Long] = None
      var end1: Option[Long] = None
      var end2: Option[Long] = None

      val m1 = for {
        _ <- IO{start1 = Some(System.nanoTime)}
        _ <- IO.sleep(2.seconds)
        _ <- IO{end1 = Some(System.nanoTime)}
      } yield List(FinalResponseEntry("A","a", 4, 1))

      val m2 = for {
        _ <- IO{start2 = Some(System.nanoTime)}
        _ <- IO.sleep(2.seconds)
        _ <- IO{end2 = Some(System.nanoTime)}
      } yield List(FinalResponseEntry("B","a", 4, 1))

      val service = new Service[IO](List()) {
        override def getResult(initReq: InitialRequest): List[(String, IO[List[FinalResponseEntry]])] = List(("A",m1),("B",m2))
      }
      val result: (Map[String, Throwable], List[FinalResponseEntry]) = service.fetchResult(InitialRequest("x", 100, 100)).unsafeRunSync()
      assert(result._1.isEmpty)
      assert(start1.get < end2.get && start2.get < end1.get)

    }

    "report failures" in {
      val service = new Service[IO](List()) {
        override def getResult(initReq: InitialRequest): List[(String, IO[List[FinalResponseEntry]])] = List(
          ("A", IO(List(FinalResponseEntry("A","a", 4, 1), FinalResponseEntry("A","b", 4, 5)))),
          ("B", IO(List(FinalResponseEntry("B","a", 4, 3), FinalResponseEntry("B","b", 4, 2)))),
          ("C", IO(List(FinalResponseEntry("C","a", 4, 4), FinalResponseEntry("C","b", 4, 6)))),
          ("D", IO.raiseError(TimeoutError)),
          ("E", IO.raiseError(ServiceUnavailable))
        )
      }
      val result: (Map[String, Throwable], List[FinalResponseEntry]) = service.fetchResult(InitialRequest("x", 100, 100)).unsafeRunSync()
      assert(result._1 == Map("D"->TimeoutError, "E"->ServiceUnavailable))
      assert(result._2.map(_.cardScore) == List(6.0,5.0,4.0,3.0,2.0,1.0))

    }

  }

}
