package zinfra.providerkits

import core.Data._
import org.http4s.Uri
import org.scalatest.WordSpec
import zinfra.providerkits.ScoredCardsProviderKitIO.{ScoredCardsRequest, ScoredCardsResponseEntry}

import scala.concurrent.duration._

class ScoredCardsProviderKitIOSpec extends WordSpec{

  val kit = ScoredCardsProviderKitIO(Uri.uri("https://google.com"), 10.seconds)

  "ScoredCardsProviderKitIO" should {
    "correctly decode normal response from provider" in {
      val result = kit.codec.decode(List(ScoredCardsResponseEntry(19.4, "ScoredCard Builder", 0.8)))

      val expected = List(FinalResponseEntry("ScoredCards", "ScoredCard Builder", 19.4, 0.212))

      assert(result.unsafeRunSync() == expected)
    }
    "assign cardScore 0 if apr is 0" in {
      val result = kit.codec.decode(List(ScoredCardsResponseEntry(0, "ScoredCard Builder", 0.8)))
      val expected = List(FinalResponseEntry("ScoredCards", "ScoredCard Builder", 0, 0))

      assert(result.unsafeRunSync() == expected)
    }
  }

}
