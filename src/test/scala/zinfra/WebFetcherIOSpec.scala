package zinfra

import cats.effect.{ContextShift, IO, Timer}
import core.Data.TimeoutError
import domain.Fetcher.{Request, Response}
import org.http4s.Uri
import org.scalatest.WordSpec

import scala.concurrent.ExecutionContext.Implicits.global

class WebFetcherIOSpec extends WordSpec{

  import org.http4s.circe.CirceEntityCodec._
  import io.circe.generic.auto._
  import scala.concurrent.duration._


  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)

  case class InMemRequest() extends Request

  case class InMemResponseEntry() extends Response

  val webFetcherIO = new WebFetcherIO[InMemRequest, InMemResponseEntry](Uri.uri("https://google.com"), 1.seconds)

  "retry function should repeat several times in there is a ConnectException" in {
    var numRetries = 0
    val result = webFetcherIO.retry(100.milliseconds, 7)(IO{numRetries += 1}.flatMap(_ => IO.raiseError(new java.net.ConnectException)))
    assertThrows[java.net.ConnectException] (result.unsafeRunSync())
    assert(numRetries == 7)
  }

  "retry function should repeat one time in there is no ConnectException exception" in {
    var numRetries = 0
    val result = webFetcherIO.retry(200.milliseconds, 4)(IO{numRetries += 1}.flatMap(_ => IO.raiseError(new Exception("something"))))
    assertThrows[Exception] (result.unsafeRunSync())
    assert(numRetries == 1)
  }

  "retry function should repeat one time in there is no any exception" in {
    var numRetries = 0
    val result = webFetcherIO.retry(200.milliseconds, 4)(IO{numRetries += 1}.flatMap(_ => IO.pure(10)))
    assert(result.unsafeRunSync() == 10)
    assert(numRetries == 1)
  }

}
