package zinfra.providerkits

import cats.effect.IO
import core.Data.{FinalResponseEntry, InitialRequest}
import domain.{Cacher, Codec, Fetcher, ProviderKit}
import org.http4s.Uri
import zinfra.{CacherIO, WebFetcherIO}

import scala.concurrent.duration.FiniteDuration
import scala.math.pow
import core.utils._

/**
 * ScoredCards specific implementation of ProviderKit interface
 */
class ScoredCardsProviderKitIO (endpoint: Uri, tmt: FiniteDuration) extends ProviderKit[IO] {

  import org.http4s.circe.CirceEntityCodec._
  import io.circe.generic.auto._
  import ScoredCardsProviderKitIO._

  val providerName = "ScoredCards"

  type Req = ScoredCardsRequest
  type Resp = ScoredCardsResponseEntry

  def codec: Codec[IO, Req, Resp] = new Codec[IO, Req, Resp] {

    /**
     * Simply map InitialRequest to ScoredCards specific request
     */
    def encode(initReq: InitialRequest): IO[ScoredCardsRequest] = IO(ScoredCardsRequest(initReq.name, initReq.creditScore, initReq.salary))

    /**
     * Transforming ScoredCards specific response into generic response and recalculating score. New score couldn't be
     * calculated if API == 0, in this case score will be assigned to 0 and such entries will appear at the end of the list
     *
     * Since ScoredCards score scale is from 0 to 1.0 and CSCards is from 0 to 10, we recalibrate ScoredCards score by
     * multiplying by 10. And to align result with the numbers in provided pdf file we multiply result by another 10
     * (we multiplied CSCards result by 10 as well)
     */
    def decode(resp: List[ScoredCardsResponseEntry]): IO[List[FinalResponseEntry]] =
      IO(resp.map(x => FinalResponseEntry(providerName, x.card, x.apr, ceil(if (x.apr != 0) x.approvalRating * pow(1/x.apr, 2) * 100 else 0, 3))))

  }


  def fetcher: Fetcher[IO, Req, Resp] = new WebFetcherIO[Req, Resp](endpoint, tmt)

  def cacher: Cacher[IO, Req, Resp] = new CacherIO[Req, Resp]
}

object ScoredCardsProviderKitIO  {

  case class ScoredCardsRequest(name: String, score: Int, salary: Int) extends domain.Fetcher.Request

  case class ScoredCardsResponseEntry(apr: Double, card: String, approvalRating: Double) extends domain.Fetcher.Response

  def apply(endpoint: Uri, tmt: FiniteDuration) = new ScoredCardsProviderKitIO(endpoint: Uri, tmt: FiniteDuration)
}