package zinfra.providerkits

import cats.effect.IO
import core.Data.{FinalResponseEntry, InitialRequest}
import domain.{Cacher, Codec, Fetcher, ProviderKit}
import org.http4s.Uri
import zinfra.{CacherIO, WebFetcherIO}

import scala.concurrent.duration.FiniteDuration
import scala.math.pow
import core.utils._

/**
 * CSCards specific implementation of ProviderKit interface
 */
class CSCardsProviderKitIO(endpoint: Uri, tmt: FiniteDuration) extends ProviderKit[IO] {

  import org.http4s.circe.CirceEntityCodec._
  import io.circe.generic.auto._
  import CSCardsProviderKitIO._

  val providerName = "CSCards"

  type Req = CSCardsRequest
  type Resp = CSCardsResponseEntry

  def codec: Codec[IO, Req, Resp] = new Codec[IO, Req, Resp] {

    /**
     * Simply map InitialRequest to CSCard specific request
     */
    def encode(initReq: InitialRequest): IO[CSCardsRequest] = IO(CSCardsRequest(initReq.name, initReq.creditScore))

    /**
     * Transforming CSCards specific response into generic response and recalculating score. New score couldn't be
     * calculated if API == 0, in this case score will be assigned to 0 and such entries will appear at the end of the list
     *
     * formula has magic the constant = 10, without it result is exactly 10 smaller then in provided pdf file
     */
    def decode(resp: List[CSCardsResponseEntry]): IO[List[FinalResponseEntry]] =
      IO(resp.map(x => FinalResponseEntry(providerName, x.cardName, x.apr, ceil(if (x.apr != 0) x.eligibility * pow(1/x.apr, 2) * 10 else 0, 3))))

  }

  def fetcher: Fetcher[IO, Req, Resp] = new WebFetcherIO[Req, Resp](endpoint, tmt)

  def cacher: Cacher[IO, Req, Resp] = new CacherIO[Req, Resp]
}

object CSCardsProviderKitIO {

  /**
   * Provider specific request
   */
  case class CSCardsRequest(name: String, creditScore: Int) extends domain.Fetcher.Request

  /**
   * Provider specific response
   */
  case class CSCardsResponseEntry(apr: Double, cardName: String, eligibility: Double) extends domain.Fetcher.Response

  def apply(endpoint: Uri, tmt: FiniteDuration) = new CSCardsProviderKitIO(endpoint: Uri, tmt: FiniteDuration)
}