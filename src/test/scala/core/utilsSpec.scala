package core

import org.scalatest.WordSpec
import cats.{Applicative, Monad, Parallel, ~>}
import cats.effect.{ContextShift, IO, Timer}
import core.Data.{AppError, TimeoutError}
import cats.effect._
import cats.syntax.all._

import scala.concurrent.duration._
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.ExecutionContext.global
import core.utils._
import java.util.concurrent.TimeUnit


class utilsSpec extends WordSpec {

  implicit val timer: Timer[IO] = IO.timer(global)
  implicit val cs: ContextShift[IO] = IO.contextShift(global)

  "The timeout function" should {
    "cut execution if it exceeds timeout" in {
      val result = timeout(2.seconds, TimeoutError)(IO.sleep(10.seconds))
      val start = System.nanoTime
        assertThrows[TimeoutError.type] (result.unsafeRunSync())
      val stop = System.nanoTime()

      val time = TimeUnit.NANOSECONDS.toSeconds(stop - start)
      assert(time < 10)
      assert(time >= 2)
    }

    "don't cut execution if it within timeout" in {
      val result = timeout(10.seconds, TimeoutError)(IO.sleep(1.seconds).map(_ => 10))
      val start = System.nanoTime
      assert(result.unsafeRunSync() == 10)
      val stop = System.nanoTime()

      val time = TimeUnit.NANOSECONDS.toSeconds(stop - start)
      assert(time < 2)
    }
  }

}
