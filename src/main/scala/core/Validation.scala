package core

import cats.MonadError
import cats.data.Validated.{Invalid, Valid}
import cats.data.{NonEmptyChain, ValidatedNec}
import core.Data.{InitialRequest, ValidationError}
import cats._
import cats.implicits._

object Validation {

  type ValidationResult[A] = ValidatedNec[String, A]

  def validateName(name: String): ValidationResult[Unit] =
    if (name.isEmpty) "Name should be not empty".invalidNec else ().validNec

  def validateScore(score: Int): ValidationResult[Unit] =
    if (score < 0 || score > 700) "Score must be within [0, 700]".invalidNec else ().validNec

  def validateSalary(salary: Int): ValidationResult[Unit] =
    if (salary < 0) "Salary can't be negative".invalidNec else ().validNec


  def validateRequest[F[_]](initialReq: InitialRequest)(implicit me: MonadError[F, Throwable]): F[Unit] = {
    def reg(a: Unit, b: Unit, c: Unit) = ()
    (
      validateName(initialReq.name),
      validateScore(initialReq.creditScore),
      validateSalary(initialReq.salary)
      ).mapN(reg)match {
      case Valid(_) => me.pure(())
      case Invalid(err) =>
        me.raiseError(ValidationError(err.toList.mkString(",")))
    }
  }

  def validatePort(port: Int): ValidationResult[Int] =
    if (port > 0 && port <= 65535) port.validNec else "Port must be in [0, 6535]".invalidNec

  /**
   * dummy uri validation. I should be amended
   */
  def validateUri(uri: String): ValidationResult[String] =
    if (uri.length > 2) uri.validNec else "Uri is not valid".invalidNec
}
