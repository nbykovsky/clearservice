package domain

import domain.Fetcher.{Request, Response}

/**
 * Abstraction of class which cashes responses from specific provider. F[_] supposed to support MonadError interface,
 * Req and Resp - types of request and responses from providers
 */
trait Cacher[F[_], Req <: Request, Resp <: Response] {

  /**
   * Writes request and response into the cache
   */
  def write(req: Req, resp: List[Resp]): F[Unit]

  /**
   * Reads values from cache. If req doesn't exists in cache it should fail (not throw)
   * computation with EntryDoesntExists error
   */
  def read(req: Req): F[List[Resp]]
}
