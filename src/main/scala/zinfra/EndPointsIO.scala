package zinfra

import cats.data.Kleisli
import cats.effect.{ContextShift, _}
import cats.implicits._
import core.Data.{InitialRequest, ValidationError}
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.blaze._
import service.Service
import core.Validation.validateRequest

/**
 * The required prats to run http4s web server
 */
trait EndPointsIO  {

  import org.http4s.circe.CirceEntityCodec._

  /**
   * Creates single endpoing (injecting Services)
   *
   * Outputs all the errors into console
   */
  def getCC(serviceIO: Service[IO])(implicit cs: ContextShift[IO], tm: Timer[IO] ): Kleisli[IO, Request[IO], Response[IO]] = HttpRoutes.of[IO] {
      case req@POST -> Root / "creditcards" =>
        val result: IO[Response[IO]] = for {
          request  <- req.as[InitialRequest]
          _        <- validateRequest[IO](request)
          response <- serviceIO.fetchResult(request)
          _        <- response._1.toList.map(e => IO(println(s"ERROR:[${e._1}] ${e._2}"))).sequence
          res      <- Ok(response._2.asJson)
        } yield res
        result.handleErrorWith(allErrHandler)
    }.orNotFound


  /**
   * Simple error handler
   */
  def allErrHandler: PartialFunction[Throwable, IO[Response[IO]]] = {
    case ValidationError(msg) => IO(println(s"ERROR: [APP] Validation $msg")) *>BadRequest(msg)
    case InvalidMessageBodyFailure(msg, _) => IO(println(s"ERROR: [APP] $msg")) *>BadRequest("Invalid Body")
    case e:Exception => IO(println(s"ERROR: [APP] Internal $e")) *> InternalServerError("Something is wrong")
  }

  /**
   * Runs http4s web server
   */
  def runner(port: Int, serviceIO: Service[IO])(implicit cs: ContextShift[IO], tm: Timer[IO] ): IO[ExitCode] =
    BlazeServerBuilder[IO]
      .bindHttp(port, "localhost")
      .withHttpApp(getCC(serviceIO))
      .serve
      .compile
      .drain
      .as(ExitCode.Success)

}


