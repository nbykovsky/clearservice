package domain

import core.Data.{FinalResponseEntry, InitialRequest}
import domain.Fetcher.{Request, Response}

/**
 * Per provider class which converts generic request and response to provider specific requests
 *
 * F[_] supposed to support MonadError interface, Req and Resp - types of request and responses from providers
 */
trait Codec[F[_], Req <: Request, Resp <: Response] {

  /**
   * Converts initial request to provider specific request
   */
  def encode(initReq: InitialRequest):F[Req]

  /**
   * Converts provider specific response to the generic response and aligns the data (scales scores)
   */
  def decode(resp: List[Resp]): F[List[FinalResponseEntry]]
}

