package core

import cats.effect.IO
import core.Validation._
import core.Data._
import org.scalatest.WordSpec

class ValidationSpec extends WordSpec{
  "Request validation" should {
    "approve correct request" in {
      assert(validateRequest[IO](InitialRequest("ab", 100, 100)).unsafeRunSync.isInstanceOf[Unit])
    }

    "trigger if name is empty" in {
      assert(validateRequest[IO](InitialRequest("", 100, 100))
        .handleErrorWith {case ValidationError(e) => IO(e)}.unsafeRunSync == "Name should be not empty")
    }

    "trigger score is not in [0, 700]" in {
      assert(validateRequest[IO](InitialRequest("ab", 701, 100))
        .handleErrorWith {case ValidationError(e) => IO(e)}.unsafeRunSync == "Score must be within [0, 700]")
    }

    "trigger salary is negative" in {
      assert(validateRequest[IO](InitialRequest("ab", 600, -1))
        .handleErrorWith {case ValidationError(e) => IO(e)}.unsafeRunSync == "Salary can't be negative")
    }

    "concatenate all errors" in {
      assert(validateRequest[IO](InitialRequest("", 701, -1))
        .handleErrorWith {case ValidationError(e) => IO(e)}.unsafeRunSync == "Name should be not empty,Score must be within [0, 700],Salary can't be negative")

    }
  }

}

